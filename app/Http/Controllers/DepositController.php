<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;

class DepositController extends Controller
{

	public function __construct()
    {
        //$this->middleware('auth');
    }

	public function indexAction($name = NULL)
	{
		switch ($name) {

			case 'privat24':
				return view('deposit.privat24');
				break;

			case 'monobank':
				return view('deposit.monobank');
				break;

			case 'easypay':
				return view('deposit.easypay');
				break;

			case 'ibox':
				return view('deposit.ibox');
				break;

			case 'vauchers':
				return view('deposit.vauchers');
				break;

			case 'offline':
				return view('deposit.offline');
				break;

			default:
				return view('deposit.common');
				break;
		}

	}
}