<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;

class CustomerHistoryController extends Controller
{

	public function __construct()
    {
        //$this->middleware('auth');
    }

	public function indexAction()
	{
		return view('history');
	}
}