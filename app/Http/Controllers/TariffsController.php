<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;

class TariffsController extends Controller
{

	public function __construct()
    {
        //$this->middleware('auth');
    }

	public function indexAction($name = NULL)
	{
		switch ($name) {

			case 'internet':
				return view('tariffs.internet');
				break;

			case 'ktv':
				return view('tariffs.ktv');
				break;

			case 'internet-plus-tv':
				return view('tariffs.internetPlusTv');
				break;

			case 'static-ip':
				return view('tariffs.staticIP');
				break;

			case 'sms':
				return view('tariffs.sms');
				break;

			case 'pause':
				return view('tariffs.pause');
				break;

			case 'relocation':
				return view('tariffs.relocation');
				break;

			default:
				return view('tariffs.common');
				break;
		}
	}
}