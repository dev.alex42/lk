<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;


class LoginController extends Controller
{
    protected $redirectTo = '/home';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function loginAction()
    {
        return view('backoffice.login');
    }

    public function loginPostAction(Request $request)
    {
        //$this->validateLogin($request);
    }
}
