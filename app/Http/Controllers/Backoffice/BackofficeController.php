<?php

namespace App\Http\Controllers\Backoffice;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class BackofficeController extends Controller
{
    public function indexAction()
    {
        return view('backoffice.main');


    }

    public function demoAction()
    {
        return view('backoffice.demo');
    }
}
