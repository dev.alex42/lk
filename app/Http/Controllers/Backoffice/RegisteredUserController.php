<?php

namespace App\Http\Controllers\Backoffice;

use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Models\Admin;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\RegisterViewResponse;


class RegisteredUserController
{
    /**
     * The guard implementation.
     *
     * @var StatefulGuard
     */
    protected $guard;

    /**
     * Create a new controller instance.
     *
     * @param StatefulGuard $guard
     * @return void
     */
    public function __construct(StatefulGuard $guard) {
        $this->guard = $guard;
    }

    /**
     * Show the registration view.
     *
     * @param Request $request
     * @return Application|Factory|RedirectResponse|View
     */
    public function create(Request $request) {

        if (Auth::guard('admin')->user()) {
            //return redirect('backoffice/dashboard-4');
            return redirect()->route('backoffice.dashboard1');
        } else {
            return view('backoffice.register');
        }
    }

    /**
     * Create a new registered user.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $expert = Admin::create([
            'name' => $request['first_name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        Auth::guard('admin')->login($expert);
        //return redirect('backoffice/dashboard-3');
        return redirect()->route('backoffice.dashboard');
    }
}
