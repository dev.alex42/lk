<?php

namespace App\Http\Controllers;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

use App\Models\User;

use Illuminate\Support\Facades\Auth;

use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /**
     * @throws GuzzleException
     * @throws ValidationException
     */
    public function loginAction(Request $request): RedirectResponse
    {
        $this->validate($request, [
            'name' => 'required',
            'password' => 'required'
        ],
        [
            'name.required' => 'Введіть логін',
            'password.required' => 'Введіть пароль'
        ]);

        $login = $request->get('name');
        $secret = $request->get('password');
        $method = 'varcode';

        $requestData = $this->APIRequest($login, $secret, $method);

        // Update or create user
        $user = User::firstOrNew(
            ['name' => $login],
            ['two_factor_secret' => $requestData->result->utoken]
        );

        $user->save();

        Auth::login($user, $remember = true);

        return redirect()->route('home');
	}

    /**
     * @throws GuzzleException
     * @throws ValidationException
     */
    private function APIRequest($login, $secret, $method)
	{
        $url = parent::T_API_URL;
        try {
            $client = new Client();
            $data = [
                "jsonrpc" => "2.0",
                "method" => $method,
                "params" => [
                    'auth' => [
                        'login' => $login,
                        'secret' => $secret
                    ]
                ],
                'id' => time()
            ];

            $response = $client->post($url, ['json' => $data]);

            $code = $response->getStatusCode();
            if ($code != 200) {
                Log::error('ERROR API REQUEST: Login:'.$login);
                throw ValidationException::withMessages(['request' => 'Помилка запросу']);
            }

            $result = json_decode($response->getBody()->getContents());

            if (isset($result->error)) {
                Log::error('ERROR API REQUEST: Error Code:'.$result->error->code.' | Error Message: '.$result->error->message);
                throw ValidationException::withMessages(['request' => 'Помилка запросу']);
            }

            return $result;
        } catch (Exception $ex) {
            Log::error('ERROR API REQUEST: Login:'.$login);
            throw ValidationException::withMessages(['request' => $ex->getMessage()]);
        }

	}


}
