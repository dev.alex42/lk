<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;

class BonusesController extends Controller
{

	public function __construct()
    {
        //$this->middleware('auth');
    }

	public function indexAction($name = NULL)
	{
		
		switch ($name) {

			case 'bonuses-program':
				return view('bonuses.bonusesProgram');
				break;

			case 'rules':
				return view('bonuses.rules');
				break;

			case 'actions':
				return view('bonuses.actions');
				break;


			default:
				return view('bonuses.common');
				break;
		}
	}
}