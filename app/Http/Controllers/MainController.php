<?php

namespace App\Http\Controllers;

//use App\Models\User;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class MainController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');

        //$this->middleware('auth.api.user');
    }


	public function indexAction()
	{


        /*
        $response = $this->requestAction('client');
        object(stdClass)497 (1) {
            ["client"]=> object(stdClass)511 (12) {
            ["id"]=> string(4) "test"
            ["uid"]=> string(6) "002401"
            ["name"]=> string(69) "Євсей-Гершен Ааронович Радомисльский"
            ["rname"]=> string(69) "Євсей-Гершен Ааронович Радомисльский"
            ["comp"]=> string(12) "Test Company"
            ["rcomp"]=> string(33) "Тестовая Компания"
            ["addr"]=> string(41) "вул. Соборна буд. 64 кв. 1"
            ["phone"]=> string(13) "+380950000103"
            ["hhone"]=> string(13) "+380950000103"
            ["url"]=> string(24) "https://dostar.unity.net"
            ["email"]=> string(21) "2003starnet@gmail.com"
            ["balance"]=> string(8) "-1001.61"
            }
        }
        ====================================================================
        $response = $this->requestAction('fields');
        object(stdClass)497 (1) {
            ["fields"]=> object(stdClass)511 (11) {
            ["id"]=> string(17) "ID клиента"
            ["uid"]=> string(3) "UID"
            ["name"]=> string(12) "Клиент"
            ["rname"]=> string(6) "Имя"
            ["comp"]=> string(16) "Название"
            ["rcomp"]=> string(10) "Фирма"
            ["addr"]=> string(10) "Адрес"
            ["phone"]=> string(22) "Осн. телефон"
            ["hhone"]=> string(22) "Доп. телефон"
            ["url"]=> string(7) "WebSite"
            ["email"]=> string(6) "E-mail"
            }
        }

        ====================================================================
        $response = $this->requestAction('info');
        object(stdClass)497 (1) {
            ["info"]=> object(stdClass)511 (12) {
            ["id"]=> string(4) "test"
            ["uid"]=> string(6) "002401"
            ["name"]=> string(69) "Євсей-Гершен Ааронович Радомисльский"
            ["rname"]=> string(69) "Євсей-Гершен Ааронович Радомисльский"
            ["comp"]=> string(12) "Test Company"
            ["rcomp"]=> string(33) "Тестовая Компания"
            ["addr"]=> string(41) "вул. Соборна буд. 64 кв. 1"
            ["phone"]=> string(13) "+380950000103"
            ["hhone"]=> string(13) "+380950000103"
            ["url"]=> string(24) "https://dostar.unity.net"
            ["email"]=> string(21) "2003starnet@gmail.com"
            ["balance"]=> string(8) "-1001.61"
            }
        }
        */
        //====================================================================
        $response = $this->requestAction('info');



        $userInfo = $response->result->info;





        $resources = $this->getResources();

        $services = $this->getServices();



        $userServices = [];



        foreach ($resources as $resource) {

            $userService = new \stdClass();

            $userService->servrname = isset($resource->servrname) ? $resource->servrname : '';

            $userService->tarif_name = isset($resource->tarif_name) ? $resource->tarif_name : '';



            $userServices[] = $userService;

            // var_dump($resource);
            // echo "<hr>";
        }




		return view('main', [
            'uid' => $userInfo->uid,
            'id' => $userInfo->id,
            'rName' => $userInfo->rname,
            'address' => $userInfo->addr,
            'phone' => $userInfo->phone,
            'email' => $userInfo->email,
            'balance' => $userInfo->balance,
            'userServices' => $userServices
        ]);
	}



    private function getUserInfo()
    {
        $userInfoData = $this->requestAction('info');


        return $userInfoData->result->info;
    }

    private function getResources()
    {
        $resourcesData = $this->requestAction('resources');


        $resourcesInfo = $resourcesData->result->resources;

        return $resourcesInfo;
    }

    private function getServices()
    {
        $resourcesData = $this->requestAction('services');

        $servicesInfo = $resourcesData->result->services;


        return $servicesInfo;
    }

    private function requestAction($method)
    {
//        $login = 'userapi';
//        $secret = 'sCI6uA9EQ9';
//        $url = 'https://dodemo.unity.net/uapi/';

        $login = 'test';
        $secret = '99999999';
        $url = env('T_API_URL');
        //la_admin
        //99999999

        //$url = 'https://user.starnet.dp.ua/uapi/';

        $url = parent::T_API_URL;

        $client = new Client();

        $data = [
            "jsonrpc" => "2.0",
            "method" => $method,
            "params" => [
                'auth' => [
                    'login' => $login,
                    'secret' => $secret
                ]
            ],
            'id' => time()
        ];

        $response = $client->post($url, ['json' => $data]);




        $code = $response->getStatusCode();



        return json_decode($response->getBody()->getContents());
    }


    /*


    ["methods"]=>
    object(stdClass)#1393 (10) {
      ["help"]=>     string(22) "Show general help info"
      ["check"]=>      string(28) "Get client's token by utoken"
      ["fields"]=>      string(32) "Show list of user profile fields"
      ["info"]=>      string(47) "Show profile info (aliases "user" and "client")"
      ["methods"]=>      string(24) "List all allowed methods"
      ["plugins"]=>      string(20) "Show list of plugins"
      ["prov"]=>      string(26) "Show clients porvider info"
      ["resources"]=>      string(27) "Show services and resources"
      ["services"]=>      string(25) "Show services desciptions"
      ["varcode"]=>      string(24) "Get users UToken (acode)"
    }


    ["resources"]=>
  object(stdClass)#1392 (2) {
    ["services"]=>
    object(stdClass)#1394 (2) {
      ["internet"]=>
      object(stdClass)#1393 (2) {
        ["id"]=>
        string(8) "internet"
        ["param"]=>
        string(7) "IP-addr"
      }
      ["mailbox"]=>
      object(stdClass)#1404 (3) {
        ["id"]=>
        string(7) "mailbox"
        ["param"]=>
        string(5) "Param"
        ["options"]=>
        object(stdClass)#1388 (2) {
          ["optA"]=>
          string(6) "Access"
          ["optM"]=>
          string(5) "Limit"
        }
      }
    }
    ["resources"]=>
    array(2) {
      [0]=>
      object(stdClass)#1395 (11) {
        ["id"]=>
        string(7) "userapi"
        ["serv"]=>
        string(8) "internet"
        ["status"]=>
        string(4) "none"
        ["host"]=>
        string(9) "127.0.0.1"
        ["servname"]=>
        string(8) "Internet"
        ["servrname"]=>
        string(16) "Интернет"
        ["paramname"]=>
        string(7) "IP-addr"
        ["param"]=>
        string(12) "192.168.70.7"
        ["tarif"]=>
        string(8) "00000200"
        ["tarif_name"]=>
        string(14) "Internet unlim"
        ["spends"]=>
        int(-35000)
      }
      [1]=>
      object(stdClass)#1397 (10) {
        ["id"]=>
        string(7) "userapi"
        ["serv"]=>
        string(7) "mailbox"
        ["status"]=>
        string(4) "none"
        ["host"]=>
        string(9) "127.0.0.1"
        ["servname"]=>
        string(7) "MailBox"
        ["servrname"]=>
        string(7) "MailBox"
        ["options"]=>
        array(2) {
          [0]=>
          object(stdClass)#1401 (4) {
            ["id"]=>
            string(1) "A"
            ["idstr"]=>
            string(4) "optA"
            ["name"]=>
            string(6) "Access"
            ["value"]=>
            string(4) "IMAP"
          }
          [1]=>
          object(stdClass)#1407 (4) {
            ["id"]=>
            string(1) "M"
            ["idstr"]=>
            string(4) "optM"
            ["name"]=>
            string(5) "Limit"
            ["value"]=>
            string(3) "10G"
          }
        }
        ["tarif"]=>
        string(8) "00000010"
        ["tarif_name"]=>
        string(14) "Обычный"
        ["spends"]=>
        int(-4516)
      }
    }
  }
  ["id"]=>
  int(1641543542)
}







    */








































	// public function indexAction()
	// {

	// 	$url = 'https://dodemo.unity.net/uapi/';

	// 	$method = 'resources';

	// 	$client = new Client();

	// 	$data = [


	// 		"jsonrpc" => "2.0",
	// 		"method" => $method,
	// 		"params" => [
	// 			'auth' => [
	// 				'login' => 'userapi',
	// 				'secret' => 'sCI6uA9EQ9'
	// 			]
	// 		],
	// 		'id' => '1641166192'
	// 	];


	// 	$response = $client->post($url, ['json' => $data]);

	// 	$code = $response->getStatusCode();
	// 	$result = $response->getBody()->getContents();

	// 	var_dump($code);
	// 	var_dump(json_decode($result));


	// }
}
