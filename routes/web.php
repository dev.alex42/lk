<?php

use App\Http\Controllers\Backoffice\AuthenticatedSessionController;
use App\Http\Controllers\Backoffice\BackofficeController;
use App\Http\Controllers\Backoffice\PaymentsController;
use App\Http\Controllers\Backoffice\RegisteredUserController;
use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Backoffice routes

//Route::get('/backoffice', [BackofficeController::class, 'demoAction'])->name('backoffice.main');

Route::group(
    [
        'prefix' => 'backoffice',
        'namespace' => 'Backoffice'
    ],
    function() {

        // Login : admin@mail.com
        // password: sCI6uA9EQ9

        Route::get('/login', [AuthenticatedSessionController::class, 'create'])->name('backoffice.login');
        Route::post('/login', [AuthenticatedSessionController::class, 'store']);

        Route::get('/logout', function() {
            return redirect()->route('backoffice.login')->with(Auth::guard('admin')->logout());
        })->name('backoffice.logout');

        Route::get('/register', [RegisteredUserController::class, 'create'])->name('backoffice.register');
        Route::post('/register', [RegisteredUserController::class, 'store']);

        Route::middleware(['backoffice'])->group(function () {
            Route::get('/', [BackofficeController::class, 'indexAction'])->name('backoffice.dashboard');

            Route::get('/demo', [BackofficeController::class, 'demoAction'])->name('backoffice.demo');

            Route::get('/payment/add', [PaymentsController::class, 'addPaymentAction'])->name('backoffice.payment.add');
            Route::post('/payment/add', [PaymentsController::class, 'addPaymentPostAction']);
            Route::post('/payment/delete', [PaymentsController::class, 'deletePaymentPostAction']);

            Route::get('/payment/{id}', [PaymentsController::class, 'paymentAction'])->name('backoffice.payment');
        });

        Route::middleware(['backoffice'])->group(function () {

        });
    }
);

// Frontend Routes
Route::get('/', 'MainController@indexAction')->name('home');

// Login : userapi
// password: sCI6uA9EQ9

Route::post('/login', 'LoginController@loginAction')->name('api.login');
Route::get('/logout', function() {

	return redirect('login')->with(Auth::logout());

})->name('get-logout');

//Route::get('/deposit/{name?}', 'DepositController@depositAction')->name('deposit');

Route::get('/deposit/{name?}', 'DepositController@indexAction')->name('deposit');

Route::get('/tariffs/{name?}', 'TariffsController@indexAction')->name('tariffs');

Route::get('/bonuses/{name?}', 'BonusesController@indexAction')->name('bonuses');

Route::get('/history', 'CustomerHistoryController@indexAction')->name('history');

Route::get('auth/google', 'Auth\LoginController@redirectToGoogle')->name('auth-google');
Route::get('auth/google/callback', 'Auth\LoginController@handleGoogleCallback')->name('auth-google-callback');

Route::get('auth/fb', 'Auth\LoginController@redirectToFacebook')->name('auth-fb');
Route::get('auth/fb/callback', 'Auth\LoginController@handleFacebookCallback')->name('auth-fb-callback');









// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');
