@extends('layouts.customer')

@section('title', 'Історія платежів')

@section('css')

@parent

<link rel="stylesheet" href="{{ mix('css/history.css') }}">

@endsection

@section('content')

<div class="content history-wrapper">

	<div class="content-title need-api">
		<div class="item">
			Історія платежів
		</div>

		<div class="item">
			Абонент: <span class="customer-name">Євсей-Гершен Ааронович Радомисльский</span>
		</div>
	</div>

	<div class="customer-account-data">
		<div class="item">
			Особовий рахунок: <span class="customer-id">123456</span>
		</div>

		<div class="item">
			Логін: <span class="customer-login">starnet</span>
		</div>
	</div>

	<div class="customer-history-table-wrapper-desktop">
		
		<table id="historyTableDesktop">
	
			<thead>
				<tr>
					<th>№</th>
					<th>Дата та час</th>
					<th>Назва платежу</th>
					<th colspan="2">Сервіс</th>
					<th>Поповнення</th>
					<th>Залишок</th>
				</tr>
			</thead>
			<tbody>

				<tr>
					<td class="number">1</td>
					<td class="datetime">01-Вер-2021 00:01</td>
					<td class="description">Нараховано за Серпень 2021</td>
					<td class="service">PPPoE</td>
					<td class="service">login: starnet</td>
					<td class="negative deposit">-105.00</td>
					<td class="balance">45.00</td>
				</tr>

				<tr>
					<td class="number">2</td>
					<td class="datetime">03-Вер-2021 00:01</td>
					<td class="description">Платіж Приватбанк</td>
					<td class="service" colspan="2">No. 253625368</td>
					<td class="deposit">80.00</td>
					<td class="balance">125.00</td>
				</tr>

				<tr>
					<td class="number">1</td>
					<td class="datetime">01-Вер-2021 00:01</td>
					<td class="description">Нараховано за Серпень 2021</td>
					<td class="service">PPPoE</td>
					<td class="service">login: starnet</td>
					<td class="negative deposit">-105.00</td>
					<td class="balance">45.00</td>
				</tr>

				<tr>
					<td class="number">2</td>
					<td class="datetime">03-Вер-2021 00:01</td>
					<td class="description">Платіж Приватбанк</td>
					<td class="service" colspan="2">No. 253625368</td>
					<td class="deposit">80.00</td>
					<td class="balance">125.00</td>
				</tr>

				<tr>
					<td class="number">1</td>
					<td class="datetime">01-Вер-2021 00:01</td>
					<td class="description">Нараховано за Серпень 2021</td>
					<td class="service">PPPoE</td>
					<td class="service">login: starnet</td>
					<td class="negative deposit">-105.00</td>
					<td class="balance">45.00</td>
				</tr>

				<tr>
					<td class="number">2</td>
					<td class="datetime">03-Вер-2021 00:01</td>
					<td class="description">Платіж Приватбанк</td>
					<td class="service" colspan="2">No. 253625368</td>
					<td class="deposit">80.00</td>
					<td class="balance">125.00</td>
				</tr>

				<tr>
					<td class="description" colspan="6">
						Залишок на 06-Лис-2021
					</td>
					<td class="balance">125.00</td>
				</tr>

				<tr>
					<td class="description" colspan="6">
						Отримано послуг в цьому місяці
					</td>
					<td class="negative balance">-25.00</td>
				</tr>

				<tr>
					<td class="description" colspan="6">
						Поточний стан рахунку (на 06-Лис-2021)
					</td>
					<td class="balance">251.00</td>
				</tr>
				
			</tbody>
			<tfoot>
				<tr>
					<th colspan="7">
						Періоди	
						<a href="">2014</a>
						<a href="">2015</a>
						<a href="">2016</a>
						<a href="">2017</a>
						<a href="">2018</a>
						<a href="">2019</a>
						<a href="">2020</a>
						<a class="active" href="">2021</a>
						<a href="">2022</a>
					</th>
				</tr>
			</tfoot>
		</table>

	</div>


</div>
	
@endsection

@section('js')

@parent

<script src="{{ mix('js/history.js') }}"></script>

@endsection