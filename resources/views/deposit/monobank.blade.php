@extends('layouts.customer')

@section('title', 'Поповнення рахунку - Монобанк')

@section('content')

<div class="content deposit-details-wrapper">
	<div class="content-title">
		Для поповнення рахунку через додаток Монобанк
	</div>

	<div class="content-title redirect">
		Вас буде направлено на сайт платіжної компанії
		<div>
			https://www.monobank.ua
		</div>
	</div>

	<div class="content-data">

		
			<ol>
				<li>
					Зайдіть у додаток Монобанк та авторизуйтеся.
				</li>
				<li>
					Оберіть "Інші Платежі" та натисніть <img class="manual-icon" src="{{ asset('assets/img/monobank_icon_1.png')}}" alt=""> Потім "Додати компанію".
				</li>
				<li>
					У рядку пошуку введіть "Старнет-Павлоград".<br>
					Потім введіть особовий рахунок або логін та натисніть "Далі". Компанію буде додано до списку оплат.<br>
					<strong>Зверніть увагу!</strong> Через додаток Монобанк можна сплатити лише за послугу Інтернет.

					<div class="test-data-wrapper">
						<div class="item">
							<div>Особовий рахунок</div>
							<div>123456</div>
						</div>
						<div class="item">
							<div>Логін</div>
							<div>starnet</div>
						</div>
					</div>

				</li>
				<li>
					Зі списку оплат оберіть "Старнет-Павлоград", введіть суму та підтвердіть платіж.
				</li>
			</ol>
		

		<p>
			Після оплати кошти будуть зараховані на Ваш особовий рахунок автоматично. Просто перевірте в історії платежів надходження цих коштів.
		</p>

		<p>
			<strong>Важливо!</strong> Зберігайте квитанцію після оплати протягом одного місяця. Це дозволить простіше вирішувати спірні питання.
		</p>

		<p>
			Зверніть увагу! Комісія за платіж НЕ стягується.
		</p>
		
	</div>

	<div class="content-bottom">
		<div class="button-wrapper">
			<a href="https://www.monobank.ua">Поповнити рахунок</a>
		</div>
	</div>

	

</div>

@endsection