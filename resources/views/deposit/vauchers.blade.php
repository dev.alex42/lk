@extends('layouts.customer')

@section('title', 'Поповнення рахунку - Ваучери поповнення')

@section('content')

<div class="content deposit-details-wrapper">
	<div class="content-title">
		Для поповнення рахунку Ваучером
	</div>

	{{-- <div class="content-title redirect">
		Вас буде направлено на сайт платіжної компанії
		<div>
			https://privat24.ua
		</div>
	</div> --}}

	<div class="content-data">

		
			<ol>
				<li>
					Зітріть захисне покриття на ваучері поповнення рахунку.
				</li>
				<li>
					Введіть номер та код ваучера у форму нижче.<br>
					Номер - 10 символів, надрукованих на ваучері.<br>
					Код - 15 символів під захисним покриттям.
				</li>
				<li>
					Натисніть кнопку "АКТИВУВАТИ"
				</li>
			</ol>
		<p>
			<strong>Важливо!</strong> Зберігайте ваучер після оплати протягом одного місяця. Це дозволить простіше вирішувати спірні питання. 
		</p>
		
	</div>

	<div class="content-bottom">
		<div class="vaucher-form-wrapper">
			<form action="">
				
				<input type="text" placeholder="Введіть номер">
				<input type="text" placeholder="Введіть код">
				<input type="submit" value="Активувати">

			</form>

			<div class="alert"></div>
		</div>
	</div>

	

</div>

@endsection