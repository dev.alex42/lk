@extends('layouts.customer')

@section('title', 'Поповнення рахунку - Privat 24')

@section('content')

<div class="content deposit-details-wrapper">
	<div class="content-title">
		Для поповнення рахунку через сайт або додаток Приват 24
	</div>

	<div class="content-title redirect">
		Вас буде направлено на сайт платіжної компанії
		<div>
			https://privat24.ua
		</div>
	</div>

	<div class="content-data">

		
			<ol>
				<li>
					Зайдіть у додаток/на сайт Приват24 та авторизуйтеся.
				</li>
				<li>
					Оберіть "Платежі". У рядку пошуку введіть "Старнет-Павлоград".
				</li>
				<li>
					Оберіть необхідну послугу (інтернет або кабельне телебачення). Рахунок можна поповнити за особовим рахунком/логіном або адресою.
					<div class="test-data-wrapper">
						<div class="item">
							<div>Особовий рахунок</div>
							<div>123456</div>
						</div>
						<div class="item">
							<div>Логін</div>
							<div>starnet</div>
						</div>
						<div class="item">
							<div>Адреса</div>
							<div>вул. Соборна, буд. 64</div>
						</div>
					</div>

				</li>
				<li>
					Введіть особовий рахунок/логін або адресу та натисніть "Продовжити".
				</li>
				<li>
					Введіть суму та підтвердіть платіж.
				</li>
			</ol>
		

		<p>
			Після оплати кошти будуть зараховані на Ваш особовий рахунок автоматично. Просто перевірте в історії платежів надходження цих коштів.
		</p>

		<p>
			<strong>Важливо!</strong> Зберігайте квитанцію після оплати протягом одного місяця. Це дозволить простіше вирішувати спірні питання.
		</p>

		<p>
			Зверніть увагу! При поповненні сервісом стягується комісія!
		</p>
		
	</div>

	<div class="content-bottom">
		<div class="button-wrapper">
			<a href="https://privat24.ua">Поповнити рахунок</a>
		</div>
	</div>

	

</div>

@endsection