@extends('layouts.customer')

@section('title', 'Поповнення рахунку - EasyPay')

@section('content')

<div class="content deposit-details-wrapper">
	<div class="content-title">
		Для поповнення рахунку через сайт EasyPay
	</div>

	<div class="content-title redirect">
		Вас буде направлено на сайт платіжної компанії
	</div>

	<div class="content-data">

		<p>
			Рахунок за інтернет або кабельне телебачення можна поповнити за особовим рахунком/логіном або адресою.
		</p>

		<div class="test-data-wrapper">
			<div class="item">
				<div>Особовий рахунок</div>
				<div>123456</div>
			</div>
			<div class="item">
				<div>Логін</div>
				<div>starnet</div>
			</div>
			<div class="item">
				<div>Адреса</div>
				<div>вул. Соборна, буд. 64</div>
			</div>
		</div>

		<div class="easypay-links-wrapper">
			<div class="item">
				<img src="{{ asset('assets/img/easypay_1.png')}}" alt="">
				<div class="button-wrapper">
					<a href="https://easypay.ua/catalog/tv/starnet-tv">Поповнити рахунок</a>
				</div>
			</div>
			<div class="item">
				<img src="{{ asset('assets/img/easypay_2.png')}}" alt="">
				<div class="button-wrapper">
					<a href="https://easypay.ua/catalog/internet/starnet-int">Поповнити рахунок</a>
				</div>
			</div>
			<div class="item">
				<img src="{{ asset('assets/img/easypay_3.png')}}" alt="">
				<div class="button-wrapper">
					<a href="https://easypay.ua/catalog/tv/starnet-tv-adress">Поповнити рахунок</a>
				</div>
			</div>
			<div class="item">
				<img src="{{ asset('assets/img/easypay_4.png')}}" alt="">
				<div class="button-wrapper">
					<a href="https://easypay.ua/catalog/internet/starnet-int-adress">Поповнити рахунок</a>
				</div>
			</div>
		</div>


		<p>
			Після оплати кошти будуть зараховані на Ваш особовий рахунок автоматично. Просто перевірте в історії платежів надходження цих коштів.
		</p>

		<p>
			<strong>Важливо!</strong> Зберігайте квитанцію після оплати протягом одного місяця. Це дозволить простіше вирішувати спірні питання.			
		</p>

		<p>
			Зверніть увагу! При поповненні сервісом стягується комісія!
		</p>
		
	</div>
	

</div>

@endsection