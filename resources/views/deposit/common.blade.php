@extends('layouts.customer')

@section('title', 'Поповнення рахунку')

@section('css')
	@parent
@endsection

@section('content')

<div class="content">
	<div class="content-title">
		Ви можете поповнити свій особовий рахунок наступними способами:
	</div>

	<div class="deposit-items-wrapper">
		<div class="item">
			<div class="title">
				Приват 24
			</div>
			<div class="button">
				<a href="{{ route('deposit', 'privat24') }}">Поповнити рахунок</a>
			</div>
		</div>
		<div class="item">
			<div class="title">
				Монобанк
			</div>
			<div class="button">
				<a href="{{ route('deposit', 'monobank') }}">Поповнити рахунок</a>
			</div>
		</div>
		<div class="item">
			<div class="title">
				Сайт EasyPay
			</div>
			<div class="button">
				<a href="{{ route('deposit', 'easypay') }}">Поповнити рахунок</a>
			</div>
		</div>
		<div class="item">
			<div class="title">
				Сайт Ibox
			</div>
			<div class="button">
				<a href="{{ route('deposit', 'ibox') }}">Поповнити рахунок</a>
			</div>
		</div>
		<div class="item">
			<div class="title">
				Ваучери поповнення
			</div>
			<div class="button">
				<a href="{{ route('deposit', 'vauchers') }}">Поповнити рахунок</a>
			</div>
		</div>
		<div class="item">
			<div class="title">
				Поповнення оффлайн
			</div>
			<div class="button">
				<a href="{{ route('deposit', 'offline') }}">Поповнити рахунок</a>
			</div>
		</div>
	</div>
</div>

@endsection

@section('js')
	@parent
@endsection