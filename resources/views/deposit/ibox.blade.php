@extends('layouts.customer')

@section('title', 'Поповнення рахунку - iBox')

@section('content')

<div class="content deposit-details-wrapper">
	<div class="content-title">
		Для поповнення рахунку через сайт iBox
	</div>

	<div class="content-title redirect">
		Вас буде направлено на сайт платіжної компанії
	</div>

	<div class="content-data">

		<p>
			Рахунок за інтернет або кабельне телебачення можна поповнити за особовим рахунком.
		</p>

		<div class="test-data-wrapper">
			<div class="item">
				<div>Особовий рахунок</div>
				<div>123456</div>
			</div>
		</div>

		<div class="ibox-links-wrapper">
			<div class="item">
				<img src="{{ asset('assets/img/ibox_1.png')}}" alt="">
				<div class="button-wrapper">
					<a href="https://ibox.ua/internet-tv-i-telefoniya-starnet-pavlograd-tv">Поповнити рахунок</a>
				</div>
			</div>
			<div class="item">
				<img src="{{ asset('assets/img/ibox_2.png')}}" alt="">
				<div class="button-wrapper">
					<a href="https://ibox.ua/starnet-pavlograd-internet">Поповнити рахунок</a>
				</div>
			</div>
		</div>


		<p>
			Після оплати кошти будуть зараховані на Ваш особовий рахунок автоматично. Просто перевірте в історії платежів надходження цих коштів.
		</p>

		<p>
			<strong>Важливо!</strong> Зберігайте квитанцію після оплати протягом одного місяця. Це дозволить простіше вирішувати спірні питання.
		</p>
		<p>
			Зверніть увагу! При поповненні сервісом стягується комісія!
		</p>
		
	</div>
	

</div>

@endsection