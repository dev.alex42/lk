@extends('layouts.customer')

@section('title', 'Тарифи та послуги - SMS-сповіщення')

@section('css')
	@parent
	<link rel="stylesheet" href="{{ mix('css/tariffs.css') }}">
@endsection

@section('content')

<div class="content">

	<div class="services-wrapper">

		<div class="content-title">
			Послуги
		</div>

		<div class="content-sub-title">
			Для вашого особового рахунку доступні такі Послуги:
		</div>

		<div class="content-sub-title not-available need-api">
			Для вашого особового рахунку ця послуга недоступна
		</div>

		<div class="item">
			<div class="item-title">
				<div class="text">SMS-Сповіщення</div>
				<div class="service-link">
					<a href="">Змінити</a>
				</div>
			</div>
			<div class="description">
				Ви можете додати або змінити Ваш номер телефону для отримання SMS-сповіщень про стан Вашого рахунку, зміну тарифного плану або будь-які інші зміни.
			</div>
			<div class="bottom">
				<a href="">Переваги SMS-сповіщення</a>
			</div>
		</div>

	</div>

</div>

@endsection