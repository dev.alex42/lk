@extends('layouts.customer')

@section('title', 'Тарифи та послуги - Статична IP-адреса')

@section('css')
	@parent
	<link rel="stylesheet" href="{{ mix('css/tariffs.css') }}">
@endsection

@section('content')

<div class="content">

	<div class="services-wrapper">

		<div class="content-title">
			Послуги
		</div>

		<div class="content-sub-title">
			Для вашого особового рахунку доступні такі Послуги:
		</div>

		<div class="content-sub-title not-available need-api">
			Для вашого особового рахунку ця послуга недоступна
		</div>

		<div class="item">
			<div class="item-title">
				<div class="text">Статична IP-адреса</div>
				<div class="service-link">
					<a href="">Замовити</a>
				</div>
			</div>
			<div class="description">
				Ви можете замовити послугу "Статична IP-адреса".
				Підключення послуги безкоштовне.
				Вартість користування послугою - 20 грн на міяць.
			</div>
			<div class="bottom">
				<a href="">Для чого потрібна Статична IP-адреса</a>
			</div>
		</div>

	</div>

</div>

@endsection