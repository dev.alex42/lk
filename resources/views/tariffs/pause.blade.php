@extends('layouts.customer')

@section('title', 'Тарифи та послуги - Пауза')

@section('css')
	@parent
	<link rel="stylesheet" href="{{ mix('css/tariffs.css') }}">
@endsection

@section('content')

<div class="content">

	<div class="services-wrapper">

		<div class="content-title">
			Послуги
		</div>

		<div class="content-sub-title">
			Для вашого особового рахунку доступні такі Послуги:
		</div>

		<div class="content-sub-title not-available need-api">
			Для вашого особового рахунку ця послуга недоступна
		</div>

		<div class="item">
			<div class="item-title">
				<div class="text">Пауза</div>
				<div class="service-link">
					<a href="">Замовити</a>
				</div>
			</div>
			<div class="description">
				Ви можете призупинити отримання послуги на період до 30 календарних днів, якщо на Вашому рахунку позитивний баланс.
			</div>
			<div class="bottom">
				<a href="">Переваги послуги "Пауза"</a>
			</div>
		</div>

	</div>

</div>

@endsection