@extends('layouts.customer')

@section('title', 'Тарифи та послуги - Інтернет + ТБ')

@section('css')
	@parent
	<link rel="stylesheet" href="{{ mix('css/tariffs.css') }}">
@endsection

@section('content')

<div class="content">

	<div class="tariffs-wrapper">

		<div class="content-title">
			Тарифи
		</div>

		<div class="content-sub-title need-api">
			Для вашого особового рахунку доступні такі Тарифи:
		</div>

		<div class="content-sub-title not-available need-api">
			Для вашого особового рахунку ця послуга недоступна
		</div>

		<div class="item internet-plus-tv">
			<div class="item-title">
				Інтернет + ТБ
			</div>
			<div class="tariffs-item-table-wrapper">
				<table>
					<thead>
						<tr>
							<th>Назва тарифу</th>
							<th>Формат</th>
							<th>Вартість, грн на місяць</th>
							<th></th>
						</tr>
					</thead>
					<tbody>

						<tr>
							<td class="title">Інтернет + ТБ</td>
							<td class="format">
								Інтернет до 100 Мбіт/сек та Телебачення у цифровому та аналоговому форматі
							</td>
							<td class="price">150</td>
							<td class="action">
								<a href="">Замовити</a>
							</td>
						</tr>

					</tbody>
				</table>
			</div>
		</div>

	</div>

</div>

@endsection