@extends('layouts.customer')

@section('title', 'Тарифи та послуги - Інтернет')

@section('css')
	@parent
	<link rel="stylesheet" href="{{ mix('css/tariffs.css') }}">
@endsection

@section('content')

<div class="content">

	<div class="tariffs-wrapper">

		<div class="content-title">
			Тарифи
		</div>

		<div class="content-sub-title need-api">
			Для вашого особового рахунку доступні такі Тарифи:
		</div>

		<div class="content-sub-title not-available need-api">
			Для вашого особового рахунку ця послуга недоступна
		</div>

		<div class="item internet">
			<div class="item-title">
				Інтернет
			</div>
			<div class="tariffs-item-table-wrapper">
				<table>
					<thead>
						<tr>
							<th>Назва тарифу</th>
							<th>Швидкість з'єднання</th>
							<th>Вартість, грн на місяць</th>
							<th></th>
						</tr>
					</thead>
					<tbody>

						<tr>
							<td class="title">Економ</td>
							<td class="speed">16 Мбіт/сек</td>
							<td class="price">75</td>
							<td class="action">
								<a href="">Замовити</a>
							</td>
						</tr>

						<tr>
							<td class="title">Стандарт</td>
							<td class="speed">60 Мбіт/сек</td>
							<td class="price">85</td>
							<td class="action">
								<a href="">Замовити</a>
							</td>
						</tr>

						<tr class="current">
							<td class="title">Преміум</td>
							<td class="speed">100 Мбіт/сек + IPTV</td>
							<td class="price">75</td>
							<td class="action">
								<a href="javascript:void(0);">Дійсний</a>
							</td>
						</tr>
					</tbody>
				</table>				
			</div>
		</div>

	</div>

</div>

@endsection