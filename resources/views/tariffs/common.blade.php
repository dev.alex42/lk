@extends('layouts.customer')

@section('title', 'Тарифи та послуги')

@section('css')
	@parent
	<link rel="stylesheet" href="{{ mix('css/tariffs.css') }}">
@endsection

@section('content')

<div class="content">

	<div class="tariffs-wrapper">

		<div class="content-title">
			Тарифи
		</div>

		<div class="content-sub-title need-api">
			Для вашого особового рахунку доступні такі Тарифи:
		</div>

		<div class="item internet">
			<div class="item-title">
				Інтернет
			</div>
			<div class="tariffs-item-table-wrapper">
				<table>
					<thead>
						<tr>
							<th>Назва тарифу</th>
							<th>Швидкість з'єднання</th>
							<th>Вартість, грн на місяць</th>
							<th></th>
						</tr>
					</thead>
					<tbody>

						<tr>
							<td class="title">Економ</td>
							<td class="speed">16 Мбіт/сек</td>
							<td class="price">75</td>
							<td class="action">
								<a href="">Замовити</a>
							</td>
						</tr>

						<tr>
							<td class="title">Стандарт</td>
							<td class="speed">60 Мбіт/сек</td>
							<td class="price">85</td>
							<td class="action">
								<a href="">Замовити</a>
							</td>
						</tr>

						<tr class="current">
							<td class="title">Преміум</td>
							<td class="speed">100 Мбіт/сек + IPTV</td>
							<td class="price">75</td>
							<td class="action">
								<a href="javascript:void(0);">Дійсний</a>
							</td>
						</tr>
					</tbody>
				</table>				
			</div>
		</div>

		<div class="item ktv">
			<div class="item-title">
				Кабельне телебачення
			</div>
			<div class="tariffs-item-table-wrapper">
				<table>
					<thead>
						<tr>
							<th>Назва пакету</th>
							<th>Формат</th>
							<th>Вартість, грн</th>
							<th></th>
						</tr>
					</thead>
					<tbody>

						<tr class="current">
							<td class="title">Базовий</td>
							<td rowspan="2" class="format">
								Цифровий та аналоговий формат
							</td>
							<td class="price">105</td>
							<td class="action">
								<a href="javascript:void(0);">Дійсний</a>
							</td>
						</tr>

						<tr>
							<td class="title">Річний</td>
							
							<td class="price">1050</td>
							<td class="action">
								<a href="">Замовити</a>
							</td>
						</tr>

						
					</tbody>
				</table>
			</div>
		</div>

		<div class="item internet-plus-tv">
			<div class="item-title">
				Інтернет + ТБ
			</div>
			<div class="tariffs-item-table-wrapper">
				<table>
					<thead>
						<tr>
							<th>Назва тарифу</th>
							<th>Формат</th>
							<th>Вартість, грн на місяць</th>
							<th></th>
						</tr>
					</thead>
					<tbody>

						<tr>
							<td class="title">Інтернет + ТБ</td>
							<td class="format">
								Інтернет до 100 Мбіт/сек та Телебачення у цифровому та аналоговому форматі
							</td>
							<td class="price">150</td>
							<td class="action">
								<a href="">Замовити</a>
							</td>
						</tr>

					</tbody>
				</table>
			</div>
		</div>
		
	</div>


	<div class="services-wrapper">

		<div class="content-title">
			Послуги
		</div>

		<div class="content-sub-title">
			Для вашого особового рахунку доступні такі Послуги:
		</div>

		<div class="item">
			<div class="item-title">
				<div class="text">Статична IP-адреса</div>
				<div class="service-link">
					<a href="">Замовити</a>
				</div>
			</div>
			<div class="description">
				Ви можете замовити послугу "Статична IP-адреса".
				Підключення послуги безкоштовне.
				Вартість користування послугою - 20 грн на міяць.
			</div>
			<div class="bottom">
				<a href="">Для чого потрібна Статична IP-адреса</a>
			</div>
		</div>

		<div class="item">
			<div class="item-title">
				<div class="text">SMS-Сповіщення</div>
				<div class="service-link">
					<a href="">Змінити</a>
				</div>
			</div>
			<div class="description">
				Ви можете додати або змінити Ваш номер телефону для отримання SMS-сповіщень про стан Вашого рахунку, зміну тарифного плану або будь-які інші зміни.
			</div>
			<div class="bottom">
				<a href="">Переваги SMS-сповіщення</a>
			</div>
		</div>

		<div class="item">
			<div class="item-title">
				<div class="text">Пауза</div>
				<div class="service-link">
					<a href="">Замовити</a>
				</div>
			</div>
			<div class="description">
				Ви можете призупинити отримання послуги на період до 30 календарних днів, якщо на Вашому рахунку позитивний баланс.
			</div>
			<div class="bottom">
				<a href="">Переваги послуги "Пауза"</a>
			</div>
		</div>

		<div class="item">
			<div class="item-title">
				<div class="text">Переїзд</div>
				<div class="service-link">
					<a href="">Замовити</a>
				</div>
			</div>
			<div class="description">
				Ви переїжджаєте на іншу адресу? Тоді замовляйте послугу "Переїзд". Баланс, особовий рахунок та логін переїдуть разом з Вами.
			</div>
			<div class="bottom">
				<a href="">Дізнатися більше про Переїзд</a>
			</div>
		</div>


		
		
		
	</div>

	

	<!--
	<div class="content-title">
		Ви можете поповнити свій особовий рахунок наступними способами:
	</div>

	<div class="deposit-items-wrapper">
		<div class="item">
			<div class="title">
				Приват 24
			</div>
			<div class="button">
				<a href="{{ route('deposit', 'privat24') }}">Поповнити рахунок</a>
			</div>
		</div>
		<div class="item">
			<div class="title">
				Монобанк
			</div>
			<div class="button">
				<a href="{{ route('deposit', 'monobank') }}">Поповнити рахунок</a>
			</div>
		</div>
		<div class="item">
			<div class="title">
				Сайт EasyPay
			</div>
			<div class="button">
				<a href="{{ route('deposit', 'easypay') }}">Поповнити рахунок</a>
			</div>
		</div>
		<div class="item">
			<div class="title">
				Сайт Ibox
			</div>
			<div class="button">
				<a href="{{ route('deposit', 'ibox') }}">Поповнити рахунок</a>
			</div>
		</div>
		<div class="item">
			<div class="title">
				Ваучери поповнення
			</div>
			<div class="button">
				<a href="{{ route('deposit', 'vauchers') }}">Поповнити рахунок</a>
			</div>
		</div>
		<div class="item">
			<div class="title">
				Поповнення оффлайн
			</div>
			<div class="button">
				<a href="{{ route('deposit', 'offline') }}">Поповнити рахунок</a>
			</div>
		</div>
	</div>

-->
</div>

@endsection

@section('js')
	@parent
@endsection