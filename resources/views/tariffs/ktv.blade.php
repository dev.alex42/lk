@extends('layouts.customer')

@section('title', 'Тарифи та послуги - Кабельне телебачення')

@section('css')
	@parent
	<link rel="stylesheet" href="{{ mix('css/tariffs.css') }}">
@endsection

@section('content')

<div class="content">

	<div class="tariffs-wrapper">

		<div class="content-title">
			Тарифи
		</div>

		<div class="content-sub-title need-api">
			Для вашого особового рахунку доступні такі Тарифи:
		</div>

		<div class="content-sub-title not-available need-api">
			Для вашого особового рахунку ця послуга недоступна
		</div>

		<div class="item ktv">
			<div class="item-title">
				Кабельне телебачення
			</div>
			<div class="tariffs-item-table-wrapper">
				<table>
					<thead>
						<tr>
							<th>Назва пакету</th>
							<th>Формат</th>
							<th>Вартість, грн</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr class="current">
							<td class="title">Базовий</td>
							<td rowspan="2" class="format">
								Цифровий та аналоговий формат
							</td>
							<td class="price">105</td>
							<td class="action">
								<a href="javascript:void(0);">Дійсний</a>
							</td>
						</tr>
						<tr>
							<td class="title">Річний</td>
							
							<td class="price">1050</td>
							<td class="action">
								<a href="">Замовити</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		
	</div>

</div>

@endsection