@extends('layouts.customer')

@section('title', 'Бонуси та Акції - Бонусна програма')

@section('css')
	@parent
	<link rel="stylesheet" href="{{ mix('css/bonuses.css') }}">
@endsection

@section('content')

<div class="content">

	<div class="bonuses-items-wrapper">

		<div class="content-title">
			Бонусна програма
		</div>

		<div class="content-sub-title need-api">
			Компанія <strong>Starnet</strong> цінує своїх постійних абонентів.<br>
			Для Вашого особового рахунку діють наступні Бонусні Програми:
		</div>

		<div class="item">
			<div class="title">
				Програма лояльності
			</div>
			<div class="sub-title">
				Розмір бонуса <span>8%</span>
			</div>

			<div class="description">
				Програма лояльності по Вашому особовому рахунку працює з 2015-10-06
			</div>
		</div>

		<div class="item">
			<div class="title">
				Програма економії
			</div>
			<div class="description">
				Програма лояльності по Вашому особовому рахунку працює з 2015-10-06
			</div>
		</div>

		<div class="bottom">
			<div class="title">
				Кількість активних днів роботи
			</div>
			<div class="description">
				3527 днів
			</div>
		</div>

		<div class="bottom-link">
			<a href="{{ route('bonuses', 'rules') }}">Правила Бонусної Програми</a>
		</div>

	</div>

</div>

@endsection