@extends('layouts.customer')

@section('title', 'Бонуси та Акції - Акційні пропозиції')

@section('css')
	@parent
	<link rel="stylesheet" href="{{ mix('css/bonuses.css') }}">
@endsection

@section('content')

<div class="content">

	<div class="actions-items-wrapper">

		<div class="content-title">
			Акційні пропозиції
		</div>

		<div class="content-sub-title">
			Компанія <strong>Starnet</strong> цінує своїх постійних абонентів.<br>
			Для Вашого особового рахунку діють наступні Акції:
		</div>

		<div class="item">
			<div class="title">
				Інтернет + ТБ <a href="">Замовити</a>
			</div>
			
			<div class="description">
				Інтернет до 100 Мбіт/сек та Кабельне телебачення у цифровому форматі
			</div>
		</div>

		<div class="bottom-link">
			<a href="">Усі Акційні пропозиції</a>
		</div>

	</div>

		
</div>

@endsection