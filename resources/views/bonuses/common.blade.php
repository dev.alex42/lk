@extends('layouts.customer')

@section('title', 'Бонуси та Акції')

@section('css')
	@parent
	<link rel="stylesheet" href="{{ mix('css/bonuses.css') }}">
@endsection

@section('content')

<div class="content">

	
	<div class="content-title bonuses-title">
		Ви можете ознайомитися зі статусом Бонусної Програми та Актуальними Акціями для Вашого особового рахунку
	</div>

	<div class="bonuses-wrapper">
		<div class="item">
			<div class="title">
				Бонусна програма
			</div>
			<div class="button">
				<a href="{{ route('bonuses', 'bonuses-program') }}">Переглянути</a>
			</div>
		</div>
		<div class="item">
			<div class="title">
				Акційні пропозиції
			</div>
			<div class="button">
				<a href="{{ route('bonuses', 'actions') }}">Переглянути</a>
			</div>
		</div>
	</div>


</div>

@endsection

@section('js')
	@parent
@endsection