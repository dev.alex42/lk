@extends('layouts.customer')

@section('title', 'Дані абонента')

@section('content')

	<div class="content">
		<div class="block bottom-bordered-block">
			<div class="item">
				<div class="title">
					Особовий рахунок
				</div>
				<div class="block-content">
					<div class="data">
						<div class="row">
							{{ $uid }}
						</div>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="title">
					Логін
				</div>
				<div class="block-content">
					<div class="data">
						<div class="row">
                            {{ $id }}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="block bottom-bordered-block">
			<div class="item">
				<div class="title">
					Замовлені послуги
				</div>
				<div class="block-content">
					<div class="data">

						@foreach ($userServices as $service)
							<div class="row service">
								{{ $service->servrname }}
							</div>
						@endforeach
					</div>
				</div>
			</div>
			<div class="item">
				<div class="title">
					Баланс <a class="title-action need-api" href="">SMS-сповіщення</a>
				</div>
				<div class="block-content">
					<div class="data">
						<div class="row balance">
							{{ $balance }} грн
						</div>
						<div class="row deposit-link">
							<a href="{{ route('deposit') }}">Поповнити рахунок</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="block">

			<div class="item tariffs-data">
				<div class="title">
					Тариф <a class="title-action need-api toggle-item-form-button" href="javascript:void(0);">Змінити тариф</a>
				</div>
				<div class="block-content">
					<div class="data">

						@foreach ($userServices as $service)

							<div class="row service">
								<div class="row tariff">
									{{ $service->servrname }}:
									@if ($service->tarif_name != '')
										{{ $service->tarif_name }}
									@endif
									<span class="small need-api">(Абонплата 150 грн в місяць)</span>
								</div>
								<div class="row sub-description need-api">
									Тарифний план діє з 01.01.2021
								</div>
							</div>
						@endforeach

					</div>
				</div>

				<div class="form-wrapper">
					<form action="">
						<input type="text" value="" placeholder="" autocomplete="off">

						<div class="form-bottom">
							<button type="submit">Зберегти</button>
							<button class="cancel-button">Закрити</button>
						</div>
					</form>
				</div>
			</div>
			<div class="item">
				<div class="title">
					Пауза <a class="title-action need-api toggle-item-form-button" href="javascript:void(0);">Призупинити</a>
				</div>
				<div class="block-content">
					<div class="data">
						<div class="row sub-description">
							Ви можете призупинити отримання послуги на період до 30 календарних днів, якщо на вашому рахунку позитивний баланс
						</div>
					</div>
				</div>

				<div class="form-wrapper">
					<form action="">
						<input type="text" value="" placeholder="" autocomplete="off">

						<div class="form-bottom">
							<button type="submit">Зберегти</button>
							<button class="cancel-button">Закрити</button>
						</div>
					</form>
				</div>
			</div>

		</div>

		<div class="block bottom-block">
			<div class="item user-data">
				<div class="title">
					Особисті дані <a class="title-action need-api toggle-item-form-button" href="javascript:void(0);">Змінити адресу</a>
				</div>
				<div class="block-content">
					<div class="data">
						<div class="row name">
							{{ $rName }}
						</div>
						<div class="row address">
							{{ $address }}
						</div>
						{{-- <div class="action-link">
							<a class="need-api toggle-item-form-button" href="javascript:void(0);">Змінити адресу</a>
						</div> --}}
						<div class="row phone">
							{{ $phone }}
						</div>
						<div class="row mail">
							{{ $email }}
						</div>
					</div>
				</div>
				<div class="form-wrapper">
					<form action="">
						<input type="text" value="{{ $address }}" placeholder="Введіть адресу" autocomplete="off">

						<div class="form-bottom">
							<button type="submit">Зберегти</button>
							<button class="cancel-button">Закрити</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

@endsection
