<x-guest-layout>
	<div id="main-container">

		<div class="login-form">
            <div class="header">
                <div class="logo">
                    <img src="{{ asset('assets/img/header-logo.png') }}" alt="Starnet Login">
                </div>
            </div>


            @if (session('status'))
	            <div class="alert alert-info">
	                {{ session('status') }}
	            </div>
	        @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    @if ($errors->has('request'))
                        {{ __('Помилка запросу') }}
                    @else
                        {{ __('Помилка') }}
                    @endif
                </div>
            @endif

            <div class="content">
                <div class="title">
                    Вхід в особистий кабінет
                </div>
                <div class="form">
                    <form action="{{ route('login') }}" method="POST">
                        @csrf

                        @if ($errors->has('name'))
                            <input class="error" type="text" name="name" placeholder="Введіть логін" autocomplete="off">
                        @else
{{--                            <input type="text" name="name" placeholder="Введіть логін" autocomplete="off" value="{{ old('name') }}">--}}
                            <input type="text" name="name" placeholder="Введіть логін" autocomplete="off" value="test">
                        @endif

                        @if ($errors->has('password'))
                            <input class="error" type="password" name="password" placeholder="Введіть пароль" autocomplete="off">
                        @else
{{--                            <input type="password" name="password" placeholder="Введіть пароль" autocomplete="off">--}}
                            <input type="password" name="password" placeholder="Введіть пароль" autocomplete="off" value="99999999">
                        @endif

                        <button type="submit">{{ __('Увійти') }}</button>
                    </form>
                </div>
                <div class="login-social">
                    <div class="title">АБО</div>
                    <div class="links">
                        <a href="{{ route('auth-fb') }}">
                            <img src="{{ asset('assets/img/fb-logo.png') }}" alt="fb">
                        </a>
                        <a href="{{ route('auth-google') }}">
                            <img src="{{ asset('assets/img/google-logo.png') }}" alt="google">
                        </a>
                    </div>
                    <div class="description">
                        Допоміжний спосіб входу в особистий кабінет
                        При першому вході через профіль FACEBOOK або GOOGLE необхідно авторизуватися
                        за допомогою логіна та пароля від особистого кабінету
                    </div>
                </div>

                <div class="remember">
                    <div class="checkbox">
                        <input class="remember-me" type="checkbox" id="remember-me" name="remember">
                        <label for="remember-me">Запам'ятати мене</label>
                    </div>
                </div>
            </div>
        </div>


	</div>
</x-guest-layout>
