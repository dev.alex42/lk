<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
	    
	    <title>{{ config('app.name', 'Laravel') }} | @yield('title')</title>

	    
	    <meta name="description" content="Описание сайта">
	    <meta name="keywords" content="Ключевые слова сайта">
	    <meta name="author" content="lysak.seiken.dp.ua">
	    
	    <link rel="shortcut icon" href="favicon.ico">

	    @section('css')
		    <link rel="stylesheet" href="{{ mix('css/main.css') }}">
		    <link rel="stylesheet" href="{{ mix('css/customer_data.css') }}">
	    @show

	</head>
<body>

	<header>
		
		<div class="logo">
			<a href="{{ route('home') }}"><img src="{{ asset('assets/img/header-logo.png')}}" alt="Logo"></a>
		</div>

		<div class="links-wrapper">
			<div class="link deposit-link">
				<a href="{{ route('deposit') }}">Поповнити рахунок</a>
			</div>

			<div class="link signout-link">
				<a href="{{ route('logout') }}">Вихід</a>
			</div>
		</div>
		
	</header>

	<div class="main">

		<div class="nav">
			<div class="list">
				<div class="item">
					<a class="{{ Request::is('/') ? 'active' : '' }}" href="{{ route('home') }}">Дані абонента</a>
				</div>
				<div class="item has-sub-menu {{ Request::is('deposit') || Request::is('deposit/*') ? 'opened' : ''}}">
					
					<a class="sub-menu-toggler {{ Request::is('deposit') || Request::is('deposit/*') ? 'active' : ''}}" href="javascript:void(0);" data-href="{{ route('deposit') }}">
						Поповнення рахунку
					</a>
					
				@if (Request::is('deposit') || Request::is('deposit/*'))
					<div class="sub-menu-wrapper" style="display: block;">
				@else 
					<div class="sub-menu-wrapper">
				@endif
						<div class="sub-menu-item">
							@if (Request::is('deposit/privat24'))
								<a class="active" href="javascript:void(0);">Приват 24</a>
							@else
								<a href="{{ route('deposit', 'privat24') }}">Приват 24</a>
							@endif
						</div>

						<div class="sub-menu-item">
							@if (Request::is('deposit/monobank'))
								<a class="active" href="javascript:void(0);">Монобанк</a>
							@else
								<a href="{{ route('deposit', 'monobank') }}">Монобанк</a>
							@endif
						</div>
						<div class="sub-menu-item">
							@if (Request::is('deposit/easypay'))
								<a class="active" href="javascript:void(0);">Сайт EasyPay</a>
							@else
								<a href="{{ route('deposit', 'easypay') }}">Сайт EasyPay</a>
							@endif
						</div>
						<div class="sub-menu-item">
							@if (Request::is('deposit/ibox'))
								<a class="active" href="javascript:void(0);">Сайт Ibox</a>
							@else
								<a href="{{ route('deposit', 'ibox') }}">Сайт Ibox</a>
							@endif
						</div>
						<div class="sub-menu-item">
							@if (Request::is('deposit/vauchers'))
								<a class="active" href="javascript:void(0);">Ваучери поповнення</a>
							@else
								<a href="{{ route('deposit', 'vauchers') }}">Ваучери поповнення</a>
							@endif
						</div>
						<div class="sub-menu-item">
							@if (Request::is('deposit/offline'))
								<a class="active" href="javascript:void(0);">Поповнення оффлайн</a>
							@else
								<a href="{{ route('deposit', 'offline') }}">Поповнення оффлайн</a>
							@endif
						</div>
						
					</div>
				</div>

				<div class="item">
					<a href="{{ route('history') }}">Історія платежів</a>
				</div>

				<div class="item has-sub-menu">

					<a class="sub-menu-toggler {{ Request::is('tariffs') || Request::is('tariffs/*') ? 'active' : ''}}" href="javascript:void(0);" data-href="{{ route('tariffs') }}">
						Тарифи та Послуги
					</a>

					@if (Request::is('tariffs') || Request::is('tariffs/*'))
						<div class="sub-menu-wrapper" style="display: block;">
					@else 
						<div class="sub-menu-wrapper">
					@endif
						
						<div class="sub-menu-item">
							@if (Request::is('tariffs/internet'))
								<a class="active" href="javascript:void(0);">Інтернет</a>
							@else
								<a href="{{ route('tariffs', 'internet') }}">Інтернет</a>
							@endif
						</div>

						<div class="sub-menu-item">
							@if (Request::is('tariffs/ktv'))
								<a class="active" href="javascript:void(0);">Кабельне телебачення</a>
							@else
								<a href="{{ route('tariffs', 'ktv') }}">Кабельне телебачення</a>
							@endif
						</div>

						<div class="sub-menu-item">
							@if (Request::is('tariffs/internet-plus-tv'))
								<a class="active" href="javascript:void(0);">Інтернет + ТБ</a>
							@else
								<a href="{{ route('tariffs', 'internet-plus-tv') }}">Інтернет + ТБ</a>
							@endif
						</div>

						<div class="sub-menu-item">
							@if (Request::is('tariffs/static-ip'))
								<a class="active" href="javascript:void(0);">Статична IP-адреса</a>
							@else
								<a href="{{ route('tariffs', 'static-ip') }}">Статична IP-адреса</a>
							@endif
						</div>

						<div class="sub-menu-item">
							@if (Request::is('tariffs/sms'))
								<a class="active" href="javascript:void(0);">SMS-сповіщення</a>
							@else
								<a href="{{ route('tariffs', 'sms') }}">SMS-сповіщення</a>
							@endif
						</div>

						<div class="sub-menu-item">
							@if (Request::is('tariffs/pause'))
								<a class="active" href="javascript:void(0);">Пауза</a>
							@else
								<a href="{{ route('tariffs', 'pause') }}">Пауза</a>
							@endif
						</div>

						<div class="sub-menu-item">
							@if (Request::is('tariffs/relocation'))
								<a class="active" href="javascript:void(0);">Переїзд</a>
							@else
								<a href="{{ route('tariffs', 'relocation') }}">Переїзд</a>
							@endif
						</div>

					</div>
				</div>

				<div class="item has-sub-menu">
					
					<a class="sub-menu-toggler {{ Request::is('bonuses') || Request::is('bonuses/*') ? 'active' : ''}}" href="javascript:void(0);" data-href="{{ route('bonuses') }}">
						Бонуси та Акції
					</a>

					@if (Request::is('bonuses') || Request::is('bonuses/*'))
						<div class="sub-menu-wrapper" style="display: block;">
					@else 
						<div class="sub-menu-wrapper">
					@endif
						<div class="sub-menu-item">
							@if (Request::is('bonuses/bonuses-program'))
								<a class="active" href="javascript:void(0);">Бонусна програма</a>
							@else
								<a href="{{ route('bonuses', 'bonuses-program') }}">Бонусна програма</a>
							@endif
						</div>
						<div class="sub-menu-item">
							@if (Request::is('bonuses/actions'))
								<a class="active" href="javascript:void(0);">Акційні пропозиції</a>
							@else
								<a href="{{ route('bonuses', 'actions') }}">Акційні пропозиції</a>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>

		@yield('content')
		
	</div>

	<div class="floating-block">
		
		<a href="#" target="_blank" class="soc-but fb" tooltip="Facebook"><i class="fab fa-facebook-f"></i></a>

		<a href="#" target="_blank" class="soc-but ig" tooltip="Instagram"><i class="fab fa-instagram"></i></a>

		<a href="#" target="_blank" class="soc-but tg" tooltip="Telegram"><i class="fab fa-telegram-plane"></i></a>

		<a href="#" target="_blank" class="soc-but wa" tooltip="Whatsapp"><i class="fab fa-whatsapp"></i></a>

		<a href="javascript:void(0);" class="soc-but marker" tooltip="Marker"><i class="fas fa-map-marker-alt"></i></a>

		<a href="javascript:void(0);" class="soc-but phone" tooltip="Phone"><i class="fas fa-phone"></i></a>

		<div id="supportData" class="support">
			<div class="title">
				Техпідтримка
			</div>
			<div class="phone-number">
				+38 095 0000 103
			</div>
			<div class="button-wrapper">
				<a href="tel:+380950000103">Подзвонити</a>
			</div>
		</div>

		<div id="addressesData" class="addresses">
			<div class="title">
				Адреси відділів
			</div>
			<div class="address">
				вул. Соборна, 64
			</div>
			<div class="address">
				вул. Дніпровська, 547
			</div>
			<div class="address">
				пр-т. Шахтобудівників, 13а
			</div>
		</div>
	</div>

	@section('js')
	    <script src="{{ mix('js/main.js') }}"></script>
    @show
	
	
</body>
</html>