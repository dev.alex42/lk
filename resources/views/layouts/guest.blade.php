<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>


        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <meta name="description" content="Описание сайта">
        <meta name="keywords" content="Ключевые слова сайта">
        <meta name="author" content="lysak.seiken.dp.ua">

        <link rel="shortcut icon" href="favicon.ico">

        <link rel="stylesheet" href="{{ mix('css/login.css') }}">


        {{--
        

        
    
    	

        <!-- Fonts -->
        <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap"> -->

        <!-- Styles -->
        <!-- <link rel="stylesheet" href="{{ mix('css/app.css') }}"> -->

        

        <!-- Scripts -->
        <!-- <script src="{{ mix('js/app.js') }}" defer></script> -->
        --}}
    </head>
    <body>
        
        {{ $slot }}
        
    </body>
</html>
