global.jquery = global.jQuery = global.$ = require('jquery');

jQuery(document).ready(function() {

	jQuery('.floating-block .soc-but.marker').on('click', function(event) {
		event.preventDefault();

		jQuery('.floating-block .soc-but.phone').removeClass('active');
		jQuery(this).toggleClass('active');

		jQuery('#supportData').hide();

		if (jQuery('#addressesData').is(':visible')) {
			jQuery('#addressesData').fadeOut();
		} else {
			jQuery('#addressesData').fadeIn();
		}
		
	});

	jQuery('.floating-block .soc-but.phone').on('click', function(event) {
		event.preventDefault();

		jQuery('.floating-block .soc-but.marker').removeClass('active');
		jQuery(this).toggleClass('active');
		
		jQuery('#addressesData').hide();
		

		if (jQuery('#supportData').is(':visible')) {
			jQuery('#supportData').fadeOut();
		} else {
			jQuery('#supportData').fadeIn();
		}
	});

	jQuery('.main .nav .has-sub-menu a.sub-menu-toggler').on('click', function(event) {

		event.preventDefault();
		event.stopPropagation();

		let isActive = jQuery(this).hasClass('active');

		if (isActive) {

			let currenSubMenu = jQuery(this).closest('.item.has-sub-menu').find('.sub-menu-wrapper');
			let currenSubMenuItem = jQuery(this).closest('.item.has-sub-menu');

			if (jQuery(currenSubMenu).is(':visible')) {

				jQuery(currenSubMenu).slideUp();
				jQuery(currenSubMenuItem).toggleClass('opened');

			} else {

				jQuery('.main .nav .has-sub-menu .sub-menu-wrapper').each(function(event) {
					jQuery(this).slideUp();
					jQuery(this).closest('.item.has-sub-menu').removeClass('opened');
				});

				jQuery(currenSubMenu).slideDown();
				jQuery(currenSubMenuItem).toggleClass('opened');

			}

		} else {

			let href = jQuery(this).data('href');

			location.href= href;

		}
		
	});

	jQuery('.deposit-details-wrapper .content-bottom .button-wrapper a').on('click', function(event) {

		event.preventDefault()

		let href = jQuery(event.target).attr('href');

		jQuery('.deposit-details-wrapper .content-title').hide();
		jQuery('.deposit-details-wrapper .content-data').hide();
		jQuery(this).hide();

		jQuery('.deposit-details-wrapper .content-title.redirect').fadeIn();

		setTimeout(function() {
			location.href = href;
		}, 3000);

	});

	jQuery('.deposit-details-wrapper .easypay-links-wrapper a').on('click', function(event) {

		event.preventDefault()

		let href = jQuery(event.target).attr('href');

		jQuery('.deposit-details-wrapper .content-title').hide();
		jQuery('.deposit-details-wrapper .content-data').hide();

		jQuery('.deposit-details-wrapper .content-title.redirect').fadeIn();

		setTimeout(function() {
			location.href = href;

			

			// jQuery('.deposit-details-wrapper .content-title').show();
			// jQuery('.deposit-details-wrapper .content-data').show();
			// jQuery('.deposit-details-wrapper .content-title.redirect').hide();

			

		}, 3000);

	});

	jQuery('.deposit-details-wrapper .ibox-links-wrapper a').on('click', function(event) {

		event.preventDefault()

		let href = jQuery(event.target).attr('href');

		jQuery('.deposit-details-wrapper .content-title').hide();
		jQuery('.deposit-details-wrapper .content-data').hide();

		jQuery('.deposit-details-wrapper .content-title.redirect').fadeIn();

		setTimeout(function() {
			location.href = href;

			// jQuery('.deposit-details-wrapper .content-title').show();
			// jQuery('.deposit-details-wrapper .content-data').show();
			// jQuery('.deposit-details-wrapper .content-title.redirect').hide();

		}, 3000);

	});

	jQuery('.main .content .block .toggle-item-form-button').on('click', function() {

		if (jQuery(this).closest('.item').find('.form-wrapper').is(':visible')) {
			jQuery(this).closest('.item').find('.form-wrapper').slideUp();
			jQuery(this).closest('.item').find('.form-wrapper form').trigger('reset');
		} else {
			jQuery(this).closest('.item').find('.form-wrapper').slideDown();
		}
		
	});

	jQuery('.main .content .block .item .form-wrapper .cancel-button').on('click', function(event) {
		event.preventDefault();
		jQuery(this).closest('.form-wrapper').slideUp();
		jQuery(this).closest('form').trigger('reset');
	});
	
});